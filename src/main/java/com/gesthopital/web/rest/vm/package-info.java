/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gesthopital.web.rest.vm;
