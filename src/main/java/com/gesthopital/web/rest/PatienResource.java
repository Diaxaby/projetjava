package com.gesthopital.web.rest;

import com.gesthopital.domain.Patien;
import com.gesthopital.repository.PatienRepository;
import com.gesthopital.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.gesthopital.domain.Patien}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PatienResource {

    private final Logger log = LoggerFactory.getLogger(PatienResource.class);

    private static final String ENTITY_NAME = "gesthopitalApiPatien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PatienRepository patienRepository;

    public PatienResource(PatienRepository patienRepository) {
        this.patienRepository = patienRepository;
    }

    /**
     * {@code POST  /patiens} : Create a new patien.
     *
     * @param patien the patien to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new patien, or with status {@code 400 (Bad Request)} if the patien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/patiens")
    public ResponseEntity<Patien> createPatien(@Valid @RequestBody Patien patien) throws URISyntaxException {
        log.debug("REST request to save Patien : {}", patien);
        if (patien.getId() != null) {
            throw new BadRequestAlertException("A new patien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Patien result = patienRepository.save(patien);
        return ResponseEntity
            .created(new URI("/api/patiens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /patiens/:id} : Updates an existing patien.
     *
     * @param id the id of the patien to save.
     * @param patien the patien to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patien,
     * or with status {@code 400 (Bad Request)} if the patien is not valid,
     * or with status {@code 500 (Internal Server Error)} if the patien couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/patiens/{id}")
    public ResponseEntity<Patien> updatePatien(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Patien patien
    ) throws URISyntaxException {
        log.debug("REST request to update Patien : {}, {}", id, patien);
        if (patien.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, patien.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!patienRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Patien result = patienRepository.save(patien);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, patien.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /patiens/:id} : Partial updates given fields of an existing patien, field will ignore if it is null
     *
     * @param id the id of the patien to save.
     * @param patien the patien to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patien,
     * or with status {@code 400 (Bad Request)} if the patien is not valid,
     * or with status {@code 404 (Not Found)} if the patien is not found,
     * or with status {@code 500 (Internal Server Error)} if the patien couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/patiens/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Patien> partialUpdatePatien(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Patien patien
    ) throws URISyntaxException {
        log.debug("REST request to partial update Patien partially : {}, {}", id, patien);
        if (patien.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, patien.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!patienRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Patien> result = patienRepository
            .findById(patien.getId())
            .map(
                existingPatien -> {
                    if (patien.getNomCompletPat() != null) {
                        existingPatien.setNomCompletPat(patien.getNomCompletPat());
                    }
                    if (patien.getEmailPat() != null) {
                        existingPatien.setEmailPat(patien.getEmailPat());
                    }
                    if (patien.getTelephonePat() != null) {
                        existingPatien.setTelephonePat(patien.getTelephonePat());
                    }
                    if (patien.getAdressePat() != null) {
                        existingPatien.setAdressePat(patien.getAdressePat());
                    }
                    if (patien.getDateNaissancePat() != null) {
                        existingPatien.setDateNaissancePat(patien.getDateNaissancePat());
                    }

                    return existingPatien;
                }
            )
            .map(patienRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, patien.getId().toString())
        );
    }

    /**
     * {@code GET  /patiens} : get all the patiens.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of patiens in body.
     */
    @GetMapping("/patiens")
    public ResponseEntity<List<Patien>> getAllPatiens(
        Pageable pageable,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Patiens");
        Page<Patien> page;
        if (eagerload) {
            page = patienRepository.findAllWithEagerRelationships(pageable);
        } else {
            page = patienRepository.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /patiens/:id} : get the "id" patien.
     *
     * @param id the id of the patien to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the patien, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/patiens/{id}")
    public ResponseEntity<Patien> getPatien(@PathVariable Long id) {
        log.debug("REST request to get Patien : {}", id);
        Optional<Patien> patien = patienRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(patien);
    }

    /**
     * {@code DELETE  /patiens/:id} : delete the "id" patien.
     *
     * @param id the id of the patien to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/patiens/{id}")
    public ResponseEntity<Void> deletePatien(@PathVariable Long id) {
        log.debug("REST request to delete Patien : {}", id);
        patienRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
