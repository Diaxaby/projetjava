package com.gesthopital.repository;

import com.gesthopital.domain.Patien;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Patien entity.
 */
@Repository
public interface PatienRepository extends JpaRepository<Patien, Long> {
    @Query(
        value = "select distinct patien from Patien patien left join fetch patien.medecins",
        countQuery = "select count(distinct patien) from Patien patien"
    )
    Page<Patien> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct patien from Patien patien left join fetch patien.medecins")
    List<Patien> findAllWithEagerRelationships();

    @Query("select patien from Patien patien left join fetch patien.medecins where patien.id =:id")
    Optional<Patien> findOneWithEagerRelationships(@Param("id") Long id);
}
