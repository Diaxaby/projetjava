package com.gesthopital.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A Patien.
 */
@Entity
@Table(name = "patien")
public class Patien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nom_complet_pat", nullable = false)
    private String nomCompletPat;

    @NotNull
    @Column(name = "email_pat", nullable = false, unique = true)
    private String emailPat;

    @NotNull
    @Column(name = "telephone_pat", nullable = false, unique = true)
    private String telephonePat;

    @Column(name = "adresse_pat")
    private String adressePat;

    @NotNull
    @Column(name = "date_naissance_pat", nullable = false)
    private LocalDate dateNaissancePat;

    @ManyToMany
    @JoinTable(
        name = "rel_patien__medecin",
        joinColumns = @JoinColumn(name = "patien_id"),
        inverseJoinColumns = @JoinColumn(name = "medecin_id")
    )
    @JsonIgnoreProperties(value = { "patiens" }, allowSetters = true)
    private Set<Medecin> medecins = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patien id(Long id) {
        this.id = id;
        return this;
    }

    public String getNomCompletPat() {
        return this.nomCompletPat;
    }

    public Patien nomCompletPat(String nomCompletPat) {
        this.nomCompletPat = nomCompletPat;
        return this;
    }

    public void setNomCompletPat(String nomCompletPat) {
        this.nomCompletPat = nomCompletPat;
    }

    public String getEmailPat() {
        return this.emailPat;
    }

    public Patien emailPat(String emailPat) {
        this.emailPat = emailPat;
        return this;
    }

    public void setEmailPat(String emailPat) {
        this.emailPat = emailPat;
    }

    public String getTelephonePat() {
        return this.telephonePat;
    }

    public Patien telephonePat(String telephonePat) {
        this.telephonePat = telephonePat;
        return this;
    }

    public void setTelephonePat(String telephonePat) {
        this.telephonePat = telephonePat;
    }

    public String getAdressePat() {
        return this.adressePat;
    }

    public Patien adressePat(String adressePat) {
        this.adressePat = adressePat;
        return this;
    }

    public void setAdressePat(String adressePat) {
        this.adressePat = adressePat;
    }

    public LocalDate getDateNaissancePat() {
        return this.dateNaissancePat;
    }

    public Patien dateNaissancePat(LocalDate dateNaissancePat) {
        this.dateNaissancePat = dateNaissancePat;
        return this;
    }

    public void setDateNaissancePat(LocalDate dateNaissancePat) {
        this.dateNaissancePat = dateNaissancePat;
    }

    public Set<Medecin> getMedecins() {
        return this.medecins;
    }

    public Patien medecins(Set<Medecin> medecins) {
        this.setMedecins(medecins);
        return this;
    }

    public Patien addMedecin(Medecin medecin) {
        this.medecins.add(medecin);
        medecin.getPatiens().add(this);
        return this;
    }

    public Patien removeMedecin(Medecin medecin) {
        this.medecins.remove(medecin);
        medecin.getPatiens().remove(this);
        return this;
    }

    public void setMedecins(Set<Medecin> medecins) {
        this.medecins = medecins;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Patien)) {
            return false;
        }
        return id != null && id.equals(((Patien) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Patien{" +
            "id=" + getId() +
            ", nomCompletPat='" + getNomCompletPat() + "'" +
            ", emailPat='" + getEmailPat() + "'" +
            ", telephonePat='" + getTelephonePat() + "'" +
            ", adressePat='" + getAdressePat() + "'" +
            ", dateNaissancePat='" + getDateNaissancePat() + "'" +
            "}";
    }
}
