package com.gesthopital.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.gesthopital.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PatienTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Patien.class);
        Patien patien1 = new Patien();
        patien1.setId(1L);
        Patien patien2 = new Patien();
        patien2.setId(patien1.getId());
        assertThat(patien1).isEqualTo(patien2);
        patien2.setId(2L);
        assertThat(patien1).isNotEqualTo(patien2);
        patien1.setId(null);
        assertThat(patien1).isNotEqualTo(patien2);
    }
}
