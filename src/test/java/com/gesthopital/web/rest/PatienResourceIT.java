package com.gesthopital.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.gesthopital.IntegrationTest;
import com.gesthopital.domain.Patien;
import com.gesthopital.repository.PatienRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PatienResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PatienResourceIT {

    private static final String DEFAULT_NOM_COMPLET_PAT = "AAAAAAAAAA";
    private static final String UPDATED_NOM_COMPLET_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_PAT = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_PAT = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_PAT = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_PAT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_NAISSANCE_PAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE_PAT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/patiens";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PatienRepository patienRepository;

    @Mock
    private PatienRepository patienRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPatienMockMvc;

    private Patien patien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patien createEntity(EntityManager em) {
        Patien patien = new Patien()
            .nomCompletPat(DEFAULT_NOM_COMPLET_PAT)
            .emailPat(DEFAULT_EMAIL_PAT)
            .telephonePat(DEFAULT_TELEPHONE_PAT)
            .adressePat(DEFAULT_ADRESSE_PAT)
            .dateNaissancePat(DEFAULT_DATE_NAISSANCE_PAT);
        return patien;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patien createUpdatedEntity(EntityManager em) {
        Patien patien = new Patien()
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);
        return patien;
    }

    @BeforeEach
    public void initTest() {
        patien = createEntity(em);
    }

    @Test
    @Transactional
    void createPatien() throws Exception {
        int databaseSizeBeforeCreate = patienRepository.findAll().size();
        // Create the Patien
        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isCreated());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeCreate + 1);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(DEFAULT_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(DEFAULT_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(DEFAULT_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(DEFAULT_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(DEFAULT_DATE_NAISSANCE_PAT);
    }

    @Test
    @Transactional
    void createPatienWithExistingId() throws Exception {
        // Create the Patien with an existing ID
        patien.setId(1L);

        int databaseSizeBeforeCreate = patienRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomCompletPatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().size();
        // set the field null
        patien.setNomCompletPat(null);

        // Create the Patien, which fails.

        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailPatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().size();
        // set the field null
        patien.setEmailPat(null);

        // Create the Patien, which fails.

        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTelephonePatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().size();
        // set the field null
        patien.setTelephonePat(null);

        // Create the Patien, which fails.

        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDateNaissancePatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().size();
        // set the field null
        patien.setDateNaissancePat(null);

        // Create the Patien, which fails.

        restPatienMockMvc
            .perform(
                post(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPatiens() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        // Get all the patienList
        restPatienMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(patien.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomCompletPat").value(hasItem(DEFAULT_NOM_COMPLET_PAT)))
            .andExpect(jsonPath("$.[*].emailPat").value(hasItem(DEFAULT_EMAIL_PAT)))
            .andExpect(jsonPath("$.[*].telephonePat").value(hasItem(DEFAULT_TELEPHONE_PAT)))
            .andExpect(jsonPath("$.[*].adressePat").value(hasItem(DEFAULT_ADRESSE_PAT)))
            .andExpect(jsonPath("$.[*].dateNaissancePat").value(hasItem(DEFAULT_DATE_NAISSANCE_PAT.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPatiensWithEagerRelationshipsIsEnabled() throws Exception {
        when(patienRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPatienMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(patienRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPatiensWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(patienRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPatienMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(patienRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getPatien() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        // Get the patien
        restPatienMockMvc
            .perform(get(ENTITY_API_URL_ID, patien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(patien.getId().intValue()))
            .andExpect(jsonPath("$.nomCompletPat").value(DEFAULT_NOM_COMPLET_PAT))
            .andExpect(jsonPath("$.emailPat").value(DEFAULT_EMAIL_PAT))
            .andExpect(jsonPath("$.telephonePat").value(DEFAULT_TELEPHONE_PAT))
            .andExpect(jsonPath("$.adressePat").value(DEFAULT_ADRESSE_PAT))
            .andExpect(jsonPath("$.dateNaissancePat").value(DEFAULT_DATE_NAISSANCE_PAT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingPatien() throws Exception {
        // Get the patien
        restPatienMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPatien() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        int databaseSizeBeforeUpdate = patienRepository.findAll().size();

        // Update the patien
        Patien updatedPatien = patienRepository.findById(patien.getId()).get();
        // Disconnect from session so that the updates on updatedPatien are not directly saved in db
        em.detach(updatedPatien);
        updatedPatien
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        restPatienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPatien.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPatien))
            )
            .andExpect(status().isOk());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(UPDATED_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(UPDATED_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(UPDATED_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(UPDATED_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    @Transactional
    void putNonExistingPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, patien.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                put(ENTITY_API_URL).with(csrf()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePatienWithPatch() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        int databaseSizeBeforeUpdate = patienRepository.findAll().size();

        // Update the patien using partial update
        Patien partialUpdatedPatien = new Patien();
        partialUpdatedPatien.setId(patien.getId());

        partialUpdatedPatien.dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        restPatienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPatien.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPatien))
            )
            .andExpect(status().isOk());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(DEFAULT_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(DEFAULT_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(DEFAULT_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(DEFAULT_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    @Transactional
    void fullUpdatePatienWithPatch() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        int databaseSizeBeforeUpdate = patienRepository.findAll().size();

        // Update the patien using partial update
        Patien partialUpdatedPatien = new Patien();
        partialUpdatedPatien.setId(patien.getId());

        partialUpdatedPatien
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        restPatienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPatien.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPatien))
            )
            .andExpect(status().isOk());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(UPDATED_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(UPDATED_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(UPDATED_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(UPDATED_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    @Transactional
    void patchNonExistingPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, patien.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isBadRequest());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPatienMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(patien))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePatien() throws Exception {
        // Initialize the database
        patienRepository.saveAndFlush(patien);

        int databaseSizeBeforeDelete = patienRepository.findAll().size();

        // Delete the patien
        restPatienMockMvc
            .perform(delete(ENTITY_API_URL_ID, patien.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Patien> patienList = patienRepository.findAll();
        assertThat(patienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
